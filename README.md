# DartsGame

## Name

This game name is: DartsVR
In an upcoming future it could be changed.

## Description

In this game you will be able to play two Darts game modes.
One of them is the 501, in this mode you will start with 501 point and every time you score some points, it will decrease. The goal is to set the score to 0.
The other one is the 301, in this mode your starting score is 0 and you need to score points until you get to 301 points.

For the moment there is no multiplayer or bots, but you will share the bar with "human" models. If a dart hit "accidentally" a human, it will start bleeding.

## Author

This game is made by: Ángel Acosta Ortega (Nijuuki_Eiji/Nijuuki's Games).

## License

None for the moment.

## Contributing

Pull requests are welcome.

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Roadmap

In development.

## Project status

In development
