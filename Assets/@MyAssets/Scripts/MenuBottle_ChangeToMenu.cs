using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuBottle_ChangeToMenu : MonoBehaviour
{
    public void ChangeToMenu()
    {
        SceneManager.LoadScene((int)ScenesNames.Scenes.Menu);
    }
    public void ChangeTo501()
    {
        SceneManager.LoadScene((int)ScenesNames.Scenes.Juego501);
    }
    public void ChangeTo321()
    {
        SceneManager.LoadScene((int)ScenesNames.Scenes.Juego321);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
