using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScenesNames
{
    public enum Scenes
    {
        Null = -1,
        Menu = 0,
        Juego501 = 1,
        Juego321 = 2
    }

    public static string GetSceneName(Scenes scene)
    {
        switch (scene)
        {
            case Scenes.Null:
                return null;
            case Scenes.Menu:
                return "MainMenu";
            case Scenes.Juego501:
                return "501";
            case Scenes.Juego321:
                return "321";
            default:
                return null;
        }
    }
}
