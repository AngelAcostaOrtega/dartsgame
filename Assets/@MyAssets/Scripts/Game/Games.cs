using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Games
{
    public enum GameModel
    {
        Model501 = 0,
        Model321 = 1
    };

    public static int GetModelPoints(GameModel model)
    {
        switch(model)
        {
            case GameModel.Model501:
                return 501;
            case GameModel.Model321:
                return 0;
            default:
                return -1;
        }
    }
}
