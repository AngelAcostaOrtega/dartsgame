using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // SINGLETON VARIABLES

    private static GameManager instance = null;
    public static GameManager Instance { get { return instance; } }

    // END SINGLETON VARIABLES

    public Games.GameModel Modelo = Games.GameModel.Model501;

    [SerializeField]
    private List<GameObject> AvaiblePlayers;

    private Dictionary<string, GameObject> players = new Dictionary<string, GameObject>();

    //[HideInInspector]
    public List<GameObject> Players = new List<GameObject>();
    private void Awake()
    {
        MakeSingleton();
        foreach(GameObject g in Players)
        {
            players.Add(g.name, g);
        }
    }
    public void SumPlayerPoints(string player, int points)
    {
        if(players.ContainsKey(player))
        {
            if (players[player].TryGetComponent(out PlayerPoints pp))
            {
                Debug.Log("El jugador " + player + " tiene " + pp.Points + " puntos.");
                if (Modelo.Equals(Games.GameModel.Model501))
                {
                    if( pp.Points - points >= 0 ) pp.SumPoints(-points);
                    else
                    {
                        // DO SOMETHING IN WRONG POINTS MADE
                    }
                    if( pp.Points.Equals(0) )
                    {
                        Victory(player);
                    }
                }
                else if (Modelo.Equals(Games.GameModel.Model321))
                {
                    Debug.Log(player + " tiene " + pp.Points);
                    if( pp.Points + points <= 321 )pp.SumPoints(points);
                    else
                    {
                        // DO SOMETHING IN WRONG POINTS MADE
                    }
                    if( pp.Points.Equals( 321 ) )
                    {
                        Victory(player);
                    }
                }
            }
                
        }
    }

    public void AddPlayer( GameObject player )
    {
        Players.Add( player );
        players.Add( player.name, player );
    }

    public void Victory(string player)
    {
        // VICTORY CODE
        Debug.Log( players[player].name + " ha ganado.");
    }

    public GameObject RequestPlayer()
    {
        GameObject result = null;

        if(AvaiblePlayers.Count > 0)
        {
            int rand = Random.Range(0, AvaiblePlayers.Count);
            result = AvaiblePlayers[rand];

            AvaiblePlayers.RemoveAt(rand);
        }

        return result;
    }


    public void UpdateScore()
    {

    }

    private void MakeSingleton()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject, 0.01f);
        }
    }
}
