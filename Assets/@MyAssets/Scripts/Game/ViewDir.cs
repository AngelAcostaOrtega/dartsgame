using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewDir : MonoBehaviour
{
    [SerializeField]
    private float yPos = 10;
    private void Update()
    {
        Debug.DrawRay(this.transform.position, this.transform.up, Color.red);
    }
    private void OnGUI()
    {
        GUI.Label(new Rect(10, yPos, 500, 200), this.name + " -> " + this.transform.up.ToString());
    }
}
