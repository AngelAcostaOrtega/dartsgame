using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPoints : MonoBehaviour
{
    private int points = 0;
    public int Points { get { return this.points; } }

    private void Start()
    {
        this.ResetPoints();
    }

    public void SumPoints(int points)
    {
        this.points += points;

        //Debug.Log(this.name + " ha marcado " + points + " puntos.");

        if(GameObject.FindGameObjectWithTag("InGame/Dardos/Diana/UI").transform.Find("Scores") != null)
        {
            GameObject scores = GameObject.FindGameObjectWithTag("InGame/Dardos/Diana/UI").transform.Find("Scores").gameObject;
            scores.transform.GetChild(0).gameObject.SetActive(true);

            GameObject linesParent = scores.transform.GetChild(0).Find("Vertical").gameObject;
            ScoreLine scoreLines = linesParent.GetComponent<ScoreLine>();

            this.name = this.name.Replace("(Clone)", "");
            scoreLines.ModifyLinePoints(this.name, points);
        }
        else
        {
            Debug.LogError("No se encontro la interfaz de la diana.");
        }
    }

    public void ResetPoints()
    {
        if(GameManager.Instance != null)
        {
            if (GameManager.Instance.Modelo.Equals(Games.GameModel.Model501)) points = Games.GetModelPoints(GameManager.Instance.Modelo);
            else points = 0;
        }
    }
}
