using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DardoAttach : MonoBehaviour
{
    private float ValidRange = 0.1f;
    private bool Colisioned = false;
    private void OnCollisionEnter(Collision _col)
    {
        if(!Colisioned)
        {
            if ( _col.gameObject.CompareTag("InGame/Dardos/Diana") )
            {
                // CODE IF COLLISION WITH DARTHBOARD

                //Debug.Log("El dardo " + this.name + " entro en colision con " + _col.gameObject.name);
                //Debug.Log(this.transform.up + " <- dardo ; diana -> " + _col.transform.up);

                //Debug.DrawRay(this.transform.position, this.transform.up, Color.green, 40f);
                //Debug.DrawRay(_col.transform.position, _col.transform.up, Color.yellow, 40f);

                float dartZ = this.transform.up.z;
                float dartX = this.transform.up.x;

                Vector2 dartZNormal = new Vector2( dartZ - ValidRange, dartZ  + ValidRange);
                Vector2 dartXNormal = new Vector2( dartX - ValidRange, dartX + ValidRange);

                //Debug.Log("Z: " + dartZNormal[0] + " -> " + dartZNormal[1] + " -> " + -_col.transform.up.z);
                //Debug.Log("X: " + dartXNormal[0] + " -> " + dartXNormal[1] + " -> " + -_col.transform.up.x);

                if ( ( dartZNormal[0] <= -_col.transform.up.z && dartZNormal[1] >= -_col.transform.up.z ) && ( dartXNormal[0] <= -_col.transform.up.x && dartXNormal[1] >= -_col.transform.up.x ) )
                {
                    // CODE IF COLLISION IS VALID;
                    if (this.TryGetComponent(out Rigidbody rb)) rb.isKinematic = true;
                    Colisioned = true;
                    if (_col.gameObject.TryGetComponent(out Point p))
                    {
                        if(GameManager.Instance != null)
                        {
                            GameObject tempPlayer = GameObject.FindGameObjectWithTag("Player");
                            tempPlayer.name = tempPlayer.name.Replace("(Clone)", "");

                            GameManager.Instance.SumPlayerPoints(GameObject.FindGameObjectWithTag("Player").name, p.Points);
                        }
                    }
                }
            }
            if ( _col.gameObject.CompareTag("InGame/Dardos/Human") )
            {
                // CODE IF COLLISION WHIT HUMAN
                if ( this.TryGetComponent(out Rigidbody rb) )
                {
                    rb.isKinematic = true;
                }
                if( this.transform.Find("Particles/Blood").TryGetComponent(out ParticleSystem ps) )
                {
                    ps.Play();
                }
                Colisioned = true;
            }
        }
    }
}
