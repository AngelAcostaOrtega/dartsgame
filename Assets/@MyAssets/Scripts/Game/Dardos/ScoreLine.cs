using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreLine : MonoBehaviour
{
    [SerializeField]
    private GameObject scoreLine;

    private Dictionary<string, GameObject> scoreLines = new Dictionary<string, GameObject>();

    public void CreateLine(string PlayerName)
    {
        GameManager manager = GameManager.Instance;
        GameObject line = scoreLine;
        TMPro.TextMeshProUGUI playerName = line.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>();
        TMPro.TextMeshProUGUI lastScore = line.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>();
        TMPro.TextMeshProUGUI totalScore = line.transform.GetChild(2).GetComponent<TMPro.TextMeshProUGUI>();

        line.name = PlayerName + "ScoreLine";
        playerName.text = PlayerName;
        lastScore.text = "0";
        totalScore.text = Games.GetModelPoints(manager.Modelo).ToString();

        Instantiate(line, this.transform);

        scoreLines.Add(PlayerName, line);
    }

    public void ModifyLinePoints(string PlayerName, int Points)
    {
        if(scoreLines.ContainsKey(PlayerName))
        {
            //Debug.Log("Encontro al jugador en el marcador");
            GameObject line = this.transform.Find(PlayerName + "ScoreLine(Clone)").gameObject;
            TMPro.TextMeshProUGUI lastScore = line.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>();
            TMPro.TextMeshProUGUI totalScore = line.transform.GetChild(2).GetComponent<TMPro.TextMeshProUGUI>();

            Debug.Log(lastScore.gameObject.name);

            if(Points > 0 ) lastScore.text = (Points).ToString();
            else lastScore.text = (-Points).ToString();

            int totalScorePoints = int.Parse(totalScore.text);
            totalScorePoints += Points;

            totalScore.text = totalScorePoints.ToString();
        }
        else
        {
            Debug.LogError("No se encontr� el jugador " + PlayerName + " en el marcador.");
        }
    }
}
