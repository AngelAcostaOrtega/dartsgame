using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDart : MonoBehaviour
{
    private Rigidbody RB;
    void Start()
    {
        RB = this.GetComponent<Rigidbody>();

        StartCoroutine(LaunchInSeconds(2));
    }

    private IEnumerator LaunchInSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        RB.velocity = this.transform.up * 5;
    }
}
