using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSelectorController : MonoBehaviour
{
    public void On501Click()
    {
        TryLoadScene(ScenesNames.Scenes.Juego501);
    }
    public void On321Click()
    {
        TryLoadScene(ScenesNames.Scenes.Juego321);
    }

    private void TryLoadScene(ScenesNames.Scenes scene)
    {
        string sceneName = ScenesNames.GetSceneName(scene);
        if ( sceneName != null)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        }
    }
}
