using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnHitOnCollision : MonoBehaviour
{
    [SerializeField]
    private GameObject normalHit = null;
    [SerializeField]
    private GameObject darthboardHit = null;

    private bool isReady = true;

    private IEnumerator ChangeReadyState()
    {
        isReady = false;
        yield return new WaitForSeconds(.5f);
        isReady = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("InGame/Dardos/Diana"))
        {
            if(darthboardHit != null && isReady)
            {
                GameObject temp = Instantiate(darthboardHit, collision.GetContact(0).point, Quaternion.identity, null);
                Destroy(temp, 4f);
                StartCoroutine(ChangeReadyState());
            }
        }
        else
        {
            if (normalHit != null && isReady)
            {
                GameObject temp = Instantiate(normalHit, collision.GetContact(0).point, Quaternion.identity, null);
                Destroy(temp, 4f);
                StartCoroutine(ChangeReadyState());
            }
        }
    }
}
