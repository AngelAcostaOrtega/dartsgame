using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersSpawn : MonoBehaviour
{
    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager.Instance;

        if(gameManager != null)
        {
            GameObject player = gameManager.RequestPlayer();
            if( player !=  null )
            {
                player.transform.position = Vector3.zero;
                player.GetComponent<PlayerPoints>().ResetPoints();
                Instantiate(player, null);
                player.name = player.name.Replace("(Clone)", "");
                gameManager.AddPlayer(player);

                if (GameObject.FindGameObjectWithTag("InGame/Dardos/Diana/UI").transform.Find("Scores") != null)
                {
                    GameObject scores = GameObject.FindGameObjectWithTag("InGame/Dardos/Diana/UI").transform.Find("Scores").gameObject;
                    scores.transform.GetChild(0).gameObject.SetActive(true);

                    GameObject linesParent = scores.transform.GetChild(0).Find("Vertical").gameObject;
                    ScoreLine scoreLines = linesParent.GetComponent<ScoreLine>();

                    scoreLines.CreateLine(player.name);
                }
            }
        }
    }
}
