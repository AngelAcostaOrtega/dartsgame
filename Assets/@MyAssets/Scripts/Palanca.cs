using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Palanca : MonoBehaviour
{
    [SerializeField]
    private GameObject dartsPrefab = null;
    private XRGrabInteractable interactor = null;
    private void OnEnable()
    {
        if(this.TryGetComponent(out XRGrabInteractable xRGrab)) interactor = xRGrab;
        if (GameObject.FindGameObjectWithTag("Darts") != null)
        {
            GameObject tempDart = GameObject.FindGameObjectWithTag("Darts");
            Destroy(tempDart, 0.1f);
            tempDart.SetActive(false);
        }
        if (dartsPrefab != null) Instantiate(dartsPrefab);
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.F))
        {
            StartCoroutine(OnLevelGrabInteraction());
        }
    }

    public void OnLevelGrab()
    {
        StartCoroutine( OnLevelGrabInteraction() );
    }
    private IEnumerator OnLevelGrabInteraction()
    {
        if (dartsPrefab != null)
        {
            if (this.TryGetComponent(out Animator anim))
            {
                anim.SetBool("Activated", true);
                yield return new WaitForEndOfFrame();
                anim.SetBool("Activated", false);
            }
            if (GameObject.FindGameObjectWithTag("Darts") != null)
            {
                GameObject tempDart = GameObject.FindGameObjectWithTag("Darts");
                Destroy(tempDart, 0.1f);
                tempDart.SetActive(false);
            }
            Instantiate(dartsPrefab);
            if (interactor != null)
            {
                if(interactor.interactorsSelecting.Count > 0 ) interactor.interactorsSelecting[0].IsSelecting(null);
            }
        }
    }
}
